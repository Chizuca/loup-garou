﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Role",menuName = "Role")]
public class Role : ScriptableObject
{
    public string roleName;
    public string description;
    
    public bool isAwake;

    public int nbVoteOnRole;
}
