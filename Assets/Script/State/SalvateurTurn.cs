﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SalvateurTurn : State
{
    public GameManagerSam listJoueur;
    private GameObject lastTouched;
    public  static GameObject chooseLastNight;
    private GameManagerSam GM;
    private GameObject panelHelp;
    public SalvateurTurn()
    {
        name = STATE.SalvateurTurn;
    }

    public override void Enter()
    {
        GM = GameManagerSam.instance;
        GM.ColorDead();
        //permet d'afficher la description du personnage sur le panel prévu a cet effet.
        panelHelp = GameObject.FindGameObjectWithTag("PanelHelp");
        panelHelp.GetComponentInChildren<TextMeshProUGUI>().text = GameObject.FindGameObjectWithTag("Salvateur").GetComponentInChildren<RoleOnPlayer>().role.description;

        listJoueur = GameManagerSam.instance;
        //permet de vérifier si le joueur est mort et si ce n'est pas le cas il lance son tour.
        foreach (var listMort in listJoueur.player)
        {
            if (listMort.GetComponentInChildren<RoleOnPlayer>().isDead)
            {
                nextState = new LoupGarouTurn();
                base.Exit();
            }
            else
            {
                Debug.Log("Le Salvateur se réveille");
                base.Enter();
            }
        }
    }

    public override void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //permet au salvateur de choisir quelqu'un à protéger.
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            foreach (var touched in results)
            {
                if (!touched.gameObject.CompareTag("Untagged") && !touched.gameObject.CompareTag("Role") && !touched.gameObject.CompareTag("Description") && !touched.gameObject.GetComponentInChildren<RoleOnPlayer>().isDead)
                {
                    if (lastTouched != null)
                    {
                        lastTouched.GetComponent<Image>().color = Color.white;
                    }
                    lastTouched = touched.gameObject;
                    touched.gameObject.GetComponent<Image>().color = Color.yellow;
                }
            }

            Debug.Log("Le Salvateur va proteger quelqun");

        }
        //permet au salvateur de valider sont tour.
        // on vérifie si il n'a pas pris la même personne que la nuit d'avant.
        if (Input.GetKeyDown(KeyCode.A) && lastTouched != null && lastTouched != chooseLastNight)
        {

            lastTouched.GetComponentInChildren<RoleOnPlayer>().isProtected = true;
            
            chooseLastNight = lastTouched;
            
            foreach (var player in GM.player)
            {
                player.gameObject.GetComponent<Image>().color = Color.white;
            }
            Debug.Log("Le Salvateur protege quelqun");
            nextState = new LoupGarouTurn();
            stage = Event.EXIT;
        }
    }

    public override void Exit()
    {
        foreach (var listMort in listJoueur.player)
        {
            if (listMort.GetComponentInChildren<RoleOnPlayer>().isDead == false)
            {
                Debug.Log("Le Salvateur s'endort");
                base.Exit();
            }
            else
            {
                base.Exit();
            }
        }
    }
}
