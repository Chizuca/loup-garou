﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayTurn : State
{
    public DayTurn()
    {
        name = STATE.DayTurn;
    }

    public override void Enter()
    {
        Debug.Log("Le Jour se lève & tout les villageois se réveillent");
        Debug.Log("L'éxcution du comdamné durant la nuit");
        base.Enter();
    }

    public override void Update()
    {
        Debug.Log("Place au vote");
        if(Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("Le vote a été décidé");
            nextState = new VoyanteTurn();
            stage = Event.EXIT;
        }
    }

    public override void Exit()
    {
        Debug.Log("L'éxcution du comdamné");
        Debug.Log("Le village s'endort");
        base.Exit();
    }
}
