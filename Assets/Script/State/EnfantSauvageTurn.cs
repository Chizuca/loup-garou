﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EnfantSauvageTurn : State
{
    public GameManagerSam listJoueur;
    public static GameObject modelChoose;
    private GameObject lastTouched;
    private GameManagerSam GM;
    public EnfantSauvageTurn()
    {
        name = STATE.EnfantSauvageTurn;
    }

    public override void Enter()
    {
        GM=GameManagerSam.instance;
        GM.ColorDead();
        listJoueur = GameManagerSam.instance;
        foreach (var listMort in listJoueur.player)
        {
            if (listMort.GetComponentInChildren<RoleOnPlayer>().isDead)
            {
                nextState = new VoyanteTurn();
                base.Exit();
            }
            else
            {
                Debug.Log("L'Enfant Sauvage se réveille");
                base.Enter();
            }
        }
        if (modelChoose == null)
        {
            base.Enter();
        }
        else if (modelChoose.GetComponentInChildren<RoleOnPlayer>().isKilled)
        {
            GameObject.FindGameObjectWithTag("WildChild").tag = "Werewolf";
            base.Exit();
        }else
        {
            nextState = new VoyanteTurn();
            base.Exit();
        }
    }

    public override void Update()
    {
        //si l'enfant sauvage n'a pas de modèle il va en choisir un.
        if (modelChoose == null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
                eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                List<RaycastResult> results = new List<RaycastResult>();
                EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
                foreach (var touched in results)
                {
                    if (lastTouched != null)
                    {
                        lastTouched.GetComponent<Image>().color=Color.white;
                    }

                    if (!touched.gameObject.CompareTag("Untagged") && !touched.gameObject.CompareTag("Role") && !touched.gameObject.CompareTag("WildChild"))
                    {
                        touched.gameObject.GetComponent<Image>().color=Color.blue;
                        lastTouched = touched.gameObject;
                    }
                    else
                    {
                        lastTouched = null;
                    }
                }
            }

        }

        Debug.Log("L'Enfant Sauvage va chosir un modèle");
        if (Input.GetKeyDown(KeyCode.A) && lastTouched != null)
        {
            foreach (var player in GM.player)
            {
                player.gameObject.GetComponent<Image>().color = Color.white;
            }

            if (modelChoose == null)
            {            
                modelChoose = lastTouched;
            } 
            Debug.Log("L'Enfant Sauvage a choisit un modèle");
            nextState = new VoyanteTurn();
            stage = Event.EXIT;
        }
    }

    public override void Exit()
    {
        foreach (var listMort in listJoueur.player)
        {
            if (listMort.GetComponentInChildren<RoleOnPlayer>().isDead == false)
            {
                Debug.Log("L'Enfant Sauvage s'endort");
                base.Exit();
            }
            else
            {
                base.Exit();
            }
        }
    }
}
