﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmoureuxTurn : State
{
    public AmoureuxTurn()
    {
        timer = 5;
        name = STATE.AmoureuxTurn;
    }

    public override void Enter()
    {
        Debug.Log("Le Couple se reveille");
        base.Enter();
    }

    public override void Update()
    {
        Debug.Log("Le couple se regarde et fond leurs voeux de mariage");
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            nextState = new EnfantSauvageTurn();
            stage = Event.EXIT;
        }
    }

    public override void Exit()
    {
        Debug.Log("Le Couple s'endort");
        base.Exit();
    }
}
