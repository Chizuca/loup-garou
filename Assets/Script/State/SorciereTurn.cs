﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SorciereTurn : State
{
    private GameManagerSam GM;
    private GameObject lastTouched;
    public GameObject panelHelp;
    public GameManagerSam listJoueur;
    public static bool potionVie;
    public static bool potionMort;
    public SorciereTurn()
    {
        name = STATE.SorciereTurn;
    }

    public override void Enter()
    {
        //permet d'afficher la description du personnage sur le panel prévu a cet effet.
        panelHelp = GameObject.FindGameObjectWithTag("PanelHelp");
        panelHelp.GetComponentInChildren<TextMeshProUGUI>().text = GameObject.FindGameObjectWithTag("Witch").GetComponentInChildren<RoleOnPlayer>().role.description;
        listJoueur = GameManagerSam.instance;
        GM = GameManagerSam.instance;
        GM.ColorDead();
        foreach (var listMort in listJoueur.player)
        {
            if (listMort.GetComponentInChildren<RoleOnPlayer>().isDead)
            {
                nextState = new CorbeauTurn();
                base.Exit();
            }
            else
            {
                //permet de montrer a la sorcière qui est mort durant la nuit.
                Debug.Log("La Sorcière se réveille");
                base.Enter();
            }
        }
    }

    public override void Update()
    {
        // la sorcière peut ensuite choisir si elle le réssucite ou si elle tue quelqu'un d'autre 
        // elle peut changer tant qu'elle n'a pas validé.
        if (Input.GetMouseButtonDown(0))
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            foreach (var touched in results)
            {
                if (touched.gameObject.CompareTag("Untagged"))
                {
                    if (lastTouched != null && !lastTouched.gameObject.GetComponentInChildren<RoleOnPlayer>().isDead)
                    {
                        lastTouched.gameObject.GetComponent<Image>().color = Color.white;
                    }
                    else if (lastTouched != null)
                    {
                        lastTouched.gameObject.GetComponent<Image>().color = Color.grey;
                    }

                    lastTouched = null;
                }
                //permet de tuer quelqu'un d'autre
                else if (!touched.gameObject.CompareTag("Description") && !touched.gameObject.CompareTag("Role") && !touched.gameObject.GetComponentInChildren<RoleOnPlayer>().isKilled && !potionMort && !touched.gameObject.GetComponentInChildren<RoleOnPlayer>().isDead)
                {
                    if (lastTouched != null && !lastTouched.gameObject.GetComponentInChildren<RoleOnPlayer>().isKilled)
                    {
                        lastTouched.gameObject.GetComponent<Image>().color = Color.white;
                    }
                    else if (lastTouched != null && lastTouched.GetComponent<Image>().color== Color.yellow)
                    {
                        lastTouched.gameObject.GetComponent<Image>().color = Color.grey;
                    }
                    touched.gameObject.GetComponent<Image>().color = Color.red;
                    lastTouched = touched.gameObject;
                }
                //permet de ressuciter la personne
                else if(!touched.gameObject.CompareTag("Description") && !touched.gameObject.CompareTag("Role") && touched.gameObject.GetComponentInChildren<RoleOnPlayer>().isKilled && !potionVie && !touched.gameObject.GetComponentInChildren<RoleOnPlayer>().isDead)
                {
                    if (lastTouched != null)
                    {
                        if (lastTouched.GetComponent<Image>().color == Color.yellow)
                        {
                            lastTouched.GetComponent<Image>().color = Color.gray;
                        }
                        else
                        {
                            lastTouched.gameObject.GetComponent<Image>().color = Color.white;

                        }
                        touched.gameObject.GetComponent<Image>().color = Color.yellow;
                        lastTouched = touched.gameObject;
                    }
                    else
                    {
                        touched.gameObject.GetComponent<Image>().color = Color.yellow;
                        lastTouched = touched.gameObject;
                    }
                }
            }
        }
        Debug.Log("La Sorcière concote ses potions");
        if (Input.GetKeyDown(KeyCode.A))
        {//permet d'utiliser le pouvoir de la sorcière et de le désactiver car elle ne peut l'utiliser qu'une fois.
            if (lastTouched != null)
            {
                if (lastTouched.GetComponent<Image>().color == Color.yellow)
                {
                    lastTouched.GetComponentInChildren<RoleOnPlayer>().isKilled = false;
                    potionVie = true;
                }
                else if(lastTouched.GetComponent<Image>().color == Color.red)
                {
                    lastTouched.GetComponentInChildren<RoleOnPlayer>().isKilled = true;
                    potionMort = true;
                }
            }
            foreach (var player in GM.player)
            {
                    player.gameObject.GetComponent<Image>().color = Color.white;
            }
            Debug.Log("La Sorcière a fait son choix");
            nextState = new CorbeauTurn();
            stage = Event.EXIT;
        }
    }

    public override void Exit()
    {
        foreach (var listJoueur in listJoueur.player)
        {
            if (listJoueur.GetComponentInChildren<RoleOnPlayer>().isDead == false)
            {
                Debug.Log("La Sorcière s'endort");
                base.Exit();
            }
            else
            {
                base.Exit();
            }
        }
    }
}
