﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEditor.SceneManagement;
using UnityEngine;

public class State
{
    public enum STATE
    {
        StartTurn,
        CupidonTurn,
        AmoureuxTurn,
        EnfantSauvageTurn,
        VoyanteTurn,
        SalvateurTurn,
        LoupGarouTurn,
        SorciereTurn,
        CorbeauTurn,
        DayTurn
        //tout les tours des roles 
    };
    public enum Event
    {
        //Quand tu rentre dans la variable , update et quand tu sors
        ENTER,
        UPDATE,
        EXIT
    };

    public STATE name;
    protected Event stage;
    protected State nextState;
    public float timer;

    public State()
    {
        stage = Event.ENTER; //demarre le tour 
    }
    public virtual void Enter()
    {
        stage = Event.UPDATE;
    }

    public virtual void Update()
    {
        stage = Event.UPDATE;
    }
    public virtual void Exit()
    {
        stage = Event.EXIT;
    }

    public State Process()
    {
        if (stage == Event.ENTER)
        {
            Enter();
        }
        if (stage == Event.UPDATE)
        {
            Update();
        }
        if (stage == Event.EXIT)
        {
            Exit();
            return nextState;
        }
        return this;
    }
}
