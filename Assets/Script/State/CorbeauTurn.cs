﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorbeauTurn : State
{
    public GameManagerSam listJoueur;
    public CorbeauTurn()
    {
        name = STATE.SalvateurTurn;
    }
    public override void Enter()
    {
        listJoueur = GameManagerSam.instance;
        foreach (var listMort in listJoueur.player)
        {
            if (listMort.GetComponentInChildren<RoleOnPlayer>().isDead)
            {
                nextState = new DayTurn();
                base.Exit();
            }
            else
            {
                Debug.Log("Le Corbeau se réveille");
                base.Enter();
            }
        }
    }

    public override void Update()
    {
        Debug.Log("La Corbeau voit d'un mauvais oeil quelqun");
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("Le Corbeau a choisit quelqun");
            nextState = new DayTurn();
            stage = Event.EXIT;
        }
    }

    public override void Exit()
    {
        foreach (var listMort in listJoueur.player)
        {
            if (listMort.GetComponentInChildren<RoleOnPlayer>().isDead == false)
            {
                Debug.Log("Le Corbeau s'endort");
                base.Exit();
            }
            else
            {
                base.Exit();
            }
        }
    }
}
