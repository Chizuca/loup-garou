﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CupidonTurn : State
{
    public GameManagerSam listJoueur;
    public CupidonTurn()
    {
        name = STATE.CupidonTurn;
    }
    public override void Enter()
    {
        listJoueur = GameManagerSam.instance;
        foreach (var listMort in listJoueur.player)
        {
            if (listMort.GetComponentInChildren<RoleOnPlayer>().isDead)
            {
                nextState = new AmoureuxTurn();
                base.Exit();
            }
            else
            {
                Debug.Log("Le Cupidon se réveille");
                base.Enter();
            }
        }
    }
    public override void Update()
    {
        Debug.Log("Le Cupidon va lié deux personne");
        if(Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("Le Cupidon a décidé de son couple");
            nextState = new AmoureuxTurn();
            stage = Event.EXIT;
        }
    }

    public override void Exit()
    {
        foreach (var listMort in listJoueur.player)
        {
            if (listMort.GetComponentInChildren<RoleOnPlayer>().isDead == false)
            {
             Debug.Log("le Cupidon s'endort");
                base.Exit();
            }
            else
            {
                base.Exit();
            }
        }
    }
}
