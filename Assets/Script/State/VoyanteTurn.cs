﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VoyanteTurn : State
{
    private GameManagerSam GM;
    private GameObject lastTouched;
    private GameObject panelHelp;

    public VoyanteTurn()
    {
        name = STATE.VoyanteTurn;
    }

    public override void Enter()
    {
        GM = GameManagerSam.instance;
        GM.ColorDead();
        //permet d'afficher la description du personnage sur le panel prévu a cet effet.
        panelHelp = GameObject.FindGameObjectWithTag("PanelHelp");
        panelHelp.GetComponentInChildren<TextMeshProUGUI>().text = GameObject.FindGameObjectWithTag("Clairvoyant").GetComponentInChildren<RoleOnPlayer>().role.description;
       
        Debug.Log("La Voyante se réveille");
        base.Enter();
    }

    public override void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //permet a la voyante de sélectionner le personnage dont elle veut voir le rôle.
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            foreach (var touched in results)
            {
                if (!touched.gameObject.CompareTag("Untagged") && !touched.gameObject.CompareTag("Role") && !touched.gameObject.CompareTag("Description") && !touched.gameObject.GetComponentInChildren<RoleOnPlayer>().isDead)
                {
                    if (lastTouched != null)
                    {
                        lastTouched.GetComponent<Image>().color=Color.white;
                    }

                    lastTouched = touched.gameObject;
                    touched.gameObject.GetComponent<Image>().color = Color.blue;
                }

            }

            Debug.Log("La voyante va observé quelqun");
        }
        //affiche le rôle sur le panel prévu a cet effet.
        if (Input.GetKeyDown(KeyCode.A) && lastTouched != null)
        {
            panelHelp.GetComponentInChildren<TextMeshProUGUI>().text = lastTouched.tag;
            foreach (var player in GM.player)
            {
                player.gameObject.GetComponent<Image>().color = Color.white;
            }

            nextState = new SalvateurTurn();
            stage = Event.EXIT;
        }
    }

    public override void Exit()
    {
        Debug.Log("La Voyante s'endort");
        base.Exit();
    }
}
