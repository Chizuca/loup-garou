﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LoupGarouTurn : State
{
    private GameObject[] LGPlayer;
    private GameManagerSam GM;
    private GameObject lastTouched;
    public GameManagerSam listJoueur;
    private GameObject panelHelp;
    public LoupGarouTurn()
    {
        name = STATE.LoupGarouTurn;
    }

    public override void Enter()
    {
        panelHelp = GameObject.FindGameObjectWithTag("PanelHelp");
        panelHelp.GetComponentInChildren<TextMeshProUGUI>().text = GameObject.FindGameObjectWithTag("Werewolf").GetComponentInChildren<RoleOnPlayer>().role.description;
        //permet de colorer en rouge tout les joueurs qui sont des loups-garous pour qu'ils puissent se reconnaître quand c'est leur tour.
        listJoueur = GameManagerSam.instance;
        GM = GameManagerSam.instance;
        GM.ColorDead();
        foreach (var listMort in listJoueur.player)
        {
            if (listMort.GetComponentInChildren<RoleOnPlayer>().isDead)
            {
                nextState = new SorciereTurn();
                base.Exit();
            }
            else
            {
                LGPlayer = GameObject.FindGameObjectsWithTag("Werewolf");
                for (int i = 0; i < LGPlayer.Length; i++)
                {
                    LGPlayer[i].GetComponent<Image>().color = Color.red;
                }
                Debug.Log("Les Loups-Garou se réveillent");
                base.Enter();
            }
        }
    }

    public override void Update()
    {
        //permet au loup-garou de choisir un joueur. il peut encore en changer tant qu'il n'a pas validé
        if (Input.GetMouseButtonDown(0))
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            foreach (var touched in results)
            {
                if (!touched.gameObject.CompareTag("Werewolf") && !touched.gameObject.CompareTag("Description") && !touched.gameObject.CompareTag("Role") &&!touched.gameObject.CompareTag("Untagged") &&  !touched.gameObject.GetComponentInChildren<RoleOnPlayer>().isDead)
                {
                    if (lastTouched != null)
                    {
                        lastTouched.gameObject.GetComponent<Image>().color = Color.white;
                    }
                    touched.gameObject.GetComponent<Image>().color = Color.blue;
                    lastTouched = touched.gameObject;
                }
            }
        }
        //permet de valider le choix du loup-garou pour montrer aux autres qu'il vote ce joueur la.
        Debug.Log("Les Loups Garous choisissent une victime");
        if(Input.GetKeyDown(KeyCode.A) && lastTouched !=null)
        {
            foreach (var player in GM.player)
            {
                player.gameObject.GetComponent<Image>().color = Color.white;
            }

            if (!lastTouched.GetComponentInChildren<RoleOnPlayer>().isProtected)
            {
                lastTouched.gameObject.GetComponentInChildren<RoleOnPlayer>().isKilled = true;

            }
            Debug.Log("Les Loups attaquent quelqun");
            nextState = new SorciereTurn();
            stage = Event.EXIT;
        }
    }

    public override void Exit()
    {
        foreach (var listMort in listJoueur.player)
        {
            if (listMort.GetComponentInChildren<RoleOnPlayer>().isDead == false)
            {
                Debug.Log("Les Loups-garou s'endorment");
                base.Exit();
            }
            else
            {
                base.Exit();
            }
        }
    }
}
