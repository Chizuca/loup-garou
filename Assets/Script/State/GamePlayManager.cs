﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayManager : MonoBehaviour
{
    private State currentState;
    void Start()
    {
        currentState = new StartTurn();
    }
    void Update()
    {
        currentState = currentState.Process();
    }
}
