﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoleOnPlayer : MonoBehaviour
{
    public Role role;
    public bool isDead;
    public bool isProtected;
    public bool isKilled;
}
