﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManagerSam : MonoBehaviour
{
    public List<Role> roles;

    public List<GameObject> player;

    public static GameManagerSam instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        //permet de distribuer les roles de manière aléatoire aux joueurs qui sont sur l'écran
        //on change ainsi la carte du joueur pour indiquer sa description 
        for (int index = 0; index < player.Count; index++)
        {
            int random = Random.Range(0, roles.Count);
            TextMeshProUGUI[] textPlayer = player[index].GetComponentsInChildren<TextMeshProUGUI>();
            if (textPlayer[0].GetComponentInChildren<TextMeshProUGUI>().CompareTag("Role"))
            {
                textPlayer[0].GetComponentInChildren<TextMeshProUGUI>().text = roles[random].roleName;
                player[index].tag = roles[random].roleName;
            }

            player[index].GetComponentInChildren<RoleOnPlayer>().role = roles[random];
            roles.RemoveAt(random);
        }
    }

    public void ColorDead()
    {
        foreach (var p in player)
        {
            if (p.gameObject.GetComponentInChildren<RoleOnPlayer>().isDead || p.gameObject.GetComponentInChildren<RoleOnPlayer>().isKilled)
            {
                p.gameObject.GetComponent<Image>().color = Color.gray;
            }
        }
    }
}
